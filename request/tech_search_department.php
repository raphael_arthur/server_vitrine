<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();
//Busca palavras-chaves que estão na categoria pesquisada
ob_clean();//clear the buffer
$keys = $database->parseToJSON("SELECT departamento.iddepartamento, departamento.nome, departamento.sigla, tecnologia_has_departamento.tecnologia_idtecnologia FROM departamento LEFT join
 tecnologia_has_departamento on departamento.iddepartamento = tecnologia_has_departamento.departamento_iddepartamento");

?>