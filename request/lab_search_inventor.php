<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();
//Busca palavras-chaves que estão na categoria pesquisada
$keys = $database->parseToJSON("SELECT inventor.idinventor, inventor.nome, laboratorio_has_inventor.laboratorio_idlaboratorio FROM inventor LEFT join
 laboratorio_has_inventor on inventor.idinventor = laboratorio_has_inventor.inventor_idinventor");

?>