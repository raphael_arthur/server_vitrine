<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

print_r($_REQUEST);
$database = new Database();
$security = new Security();

$name = '"'.$database->real_escape_string($_REQUEST["name"]).'"';
$desc = '"'.$database->real_escape_string($_REQUEST["desc"]).'"';
$dados = '"'.$database->real_escape_string($_REQUEST["dados"]).'"';
//$idprojeto = '"'.$database->real_escape_string($_REQUEST["idprojeto"]).'"';
$idtech = '"'.$database->real_escape_string($_REQUEST["idtech"]).'"';
$idusuario = '"'.$database->real_escape_string($_REQUEST["idusuario"]).'"';

$oldDep = '"'.$database->real_escape_string($_REQUEST["oldDepartment"]).'"';
$oldInventor = '"'.$database->real_escape_string($_REQUEST["oldinventor"]).'"';
$oldKey = '"'.$database->real_escape_string($_REQUEST["oldKey"]).'"';
$oldLab = '"'.$database->real_escape_string($_REQUEST["oldLab"]).'"';

$idInventorArray = explode('|',$_REQUEST["idinventor"]);
$idDepartamentoArray = explode('|',$_REQUEST["iddepartamento"]);
$idKeyWordArray = explode('|',$_REQUEST["idpalavra_chave"]);
$idLabArray = explode('|',$_REQUEST["idLab"]);


/*a linha abaixo seta os dados de texto da tecnologia atualizada*/
$editrow = $database->query("UPDATE tecnologia SET nome=".$name.", descricao=".$desc.", dados_protecao=".$dados.", usuario_idusuario=".$idusuario." WHERE idtecnologia=".$idtech);
if($editrow){
  echo "Sucesso: editrow corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}


/*
guilherme natal 22/02/2018
as linhas abaixo fazem tanto o update de departaemento quanto de inventor
*/

$editDepartamento = $database->query("DELETE FROM tecnologia_has_departamento WHERE tecnologia_idtecnologia=".$idtech);
for ($i = 0; $i < count($idDepartamentoArray) -1 ; $i++) { 
  $iddepartamento = '"'.$database->real_escape_string($idDepartamentoArray[$i]).'"';
  $insertDepartamento = $database->query("INSERT INTO tecnologia_has_departamento (tecnologia_idtecnologia,departamento_iddepartamento) VALUES (".$idtech.",".$iddepartamento.")");
}

$editInventor = $database->query("DELETE FROM tecnologia_has_inventor WHERE tecnologia_idtecnologia=".$idtech);
for ($i = 0; $i < count($idInventorArray) -1 ; $i++) {
  $idinventor = '"'.$database->real_escape_string($idInventorArray[$i]).'"';
  $insertInventor = $database->query("INSERT INTO tecnologia_has_inventor (tecnologia_idtecnologia, inventor_idinventor) VALUES (".$idtech.",".$idinventor.")");
}


$editPalavraChave = $database->query("DELETE FROM tecnologia_has_palavra_chave WHERE tecnologia_idtecnologia=".$idtech);
for ($i = 0; $i < count($idKeyWordArray) -1 ; $i++) {
  $idpalavra_chave = '"'.$database->real_escape_string($idKeyWordArray[$i]).'"';
  $insertInventor = $database->query("INSERT INTO tecnologia_has_palavra_chave (tecnologia_idtecnologia, palavra_chave_idpalavra_chave) VALUES (".$idtech.",".$idpalavra_chave.")");
}


$editLab = $database->query("DELETE FROM tecnologia_has_laboratorio WHERE tecnologia_idtecnologia=".$idtech);
for ($i = 0; $i < count($idLabArray) -1 ; $i++) {
  $idLab = '"'.$database->real_escape_string($idLabArray[$i]).'"';
  $insertInventor = $database->query("INSERT INTO tecnologia_has_laboratorio (tecnologia_idtecnologia, laboratorio_idlaboratorio) VALUES (".$idtech.",".$idLab.")");
}



//$statement = $database->prepare($insert_row);
