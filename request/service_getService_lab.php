<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();

$idlab = '"'.$database->real_escape_string($_REQUEST["idlab"]).'"';


//$categories = $database->parseToJSON("SELECT * FROM (servico INNER JOIN subservico ON servico.idservico = subservico.idservico) WHERE servico.idlaboratorio = ".$idlab);
$categories = $database->parseToJSON("SELECT * FROM (servico INNER JOIN subservico ON servico.idservicos = subservico.idservico) WHERE servico.idlaboratorio = ".$idlab);
