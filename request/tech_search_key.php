<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();
//Busca palavras-chaves que estão na categoria pesquisada
ob_clean();//clear the buffer
$keys = $database->parseToJSON("SELECT palavra_chave.idpalavra_chave, palavra_chave.nome, tecnologia_has_palavra_chave.tecnologia_idtecnologia FROM palavra_chave LEFT join
 tecnologia_has_palavra_chave on palavra_chave.idpalavra_chave = tecnologia_has_palavra_chave.palavra_chave_idpalavra_chave");

?>