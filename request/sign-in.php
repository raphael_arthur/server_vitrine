<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin:*");

try{
	if(isset($_POST["email"]) and $_POST["password"] != "") {
	    $User = new User();
	    print $User->signin($_POST["email"], $_POST["password"]);
    } else {
        print "Undefined Error.";
    }
}
catch(PDOException $e){
    echo $e->getMessage();
 }

?>
