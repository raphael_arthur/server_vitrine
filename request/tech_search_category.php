<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();

ob_clean();/*limpa o buffer*/

$id = '"'.$database->real_escape_string($_REQUEST["id"]).'"';
//Busca palavras-chaves que estão na categoria pesquisada
$keys = $database->parseToJSON2("SELECT idpalavra_chave FROM palavra_chave WHERE categoria_idcategoria =".$id);
$keys = json_decode($keys, true);

//Busca quais tecnologias possuem as palavras-chave pesquisada
$techs_id[0] =0;
if(!empty($keys)){
    $i = 0;

    foreach($keys as $key){
        $temp = $database->parseToJSON2("SELECT tecnologia_idtecnologia FROM tecnologia_has_palavra_chave WHERE palavra_chave_idpalavra_chave =".$key["idpalavra_chave"]);
        $tech_keys = json_decode($temp,true);
        foreach ($tech_keys as $tech_id_key) {/*para cada tecnlogia encontrada este loop verifica se ela ja existe*/
            if(!array_search($tech_id_key,$techs_id)){/*se não existir adiciona ao array se existir decarta*/
                $techs_id[$i] = $tech_id_key['tecnologia_idtecnologia'];
                $i++;                
            }
        }
    }
    $ctrl = 0;
    if(!empty($techs_id)){
        foreach($techs_id as $id_tech){
            $temp = $database->parseToJSON2("SELECT * FROM tecnologia WHERE idtecnologia = ".$id_tech);
            if($ctrl == 0){
                $techs = json_decode($temp, true);
                $ctrl = 1;
            }
            else{
                $temp_decode = json_decode($temp, true);
                if(!empty($temp_decode))
                    array_push($techs, $temp_decode[0]);                
            }
        }
        echo json_encode($techs);
    }
}

?>