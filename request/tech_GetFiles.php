/*Guilherme Natal: 15/03/2018
este script recebe o id de uma tecnologia e 
retorna todos os seus arquivos em formato base64

*/

<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$idtech = $_POST['idtech'];
$database = new Database();
$security = new Security();

ob_clean();/*clears the body*/

$query = "SELECT (path) FROM tecnolgia_arquivos WHERE idtech='$idtech'";
$result = $database->query($query);

$array = array();

while($row = $result->fetch_assoc()){
	$array[] = (
		'image64' => base64_encode(file_get_contents($row['path']));
	);
}


echo json_encode($array);

?>