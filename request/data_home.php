<?php
    /**
	 * Serves a JSON formated data to the requester.
	 */

	// Defines the return type.
	header('Content-Type: application/json');
    
    require_once "../class/Database.class.php";
    require_once "../class/ui/UserInterface.class.php";
    $Database = new Database();

    if(isset($_POST["userToken"])){
        $userToken = $_POST["userToken"];
    }

    session_start();
    $userID = $_SESSION["userID"];
    session_write_close();

    //$ui = new UserInterface();
    //$ui->updateUi($_POST["userToken"], $userID);
    $name = $Database->selectSingleton("SELECT nome FROM user WHERE iduser='$userID'","nome");

    //print $result;
    print '{
            "attr": {
                "spf_menu_title": {
                    "photo": "img/users/user_' . $userID . '.jpg",
                    "name": "' . $name . '",
                    "email": "' . $_SESSION["userEmail"] . '"
                }
            },
            "body": {
                "spf_dialog_name": "' . $name . '",
                "spf_teste": "George muleque doido",
                "spf_center_content": "<h1>div centralizado</h1> ID Usuário:",
                "spf_whatsapp": "(38) 98845-0122",
                "spf_email": "curvadorio@uol.com.br",

                "spf_city": "O Hotel Fazenda Curva do Rio fica localizado em Unaí, cidade polo do Noroeste Mineiro, uma região privilegiada por estar à 180 km da Capital Federal.",
                "spf_city_about": "Unaí é uma cidade com aproximadamente 80 mil habitantes, contando com completa infraestrutura para receber eventos e atividades turísticas, além de contar com diversas festas regionais, realizadas durante o decorrer do ano."
            }
        }';
?>