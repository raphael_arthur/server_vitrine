<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

print_r($_REQUEST);
$database = new Database();
$security = new Security();

$name = '"'.$database->real_escape_string($_REQUEST["name"]).'"';
$desc = '"'.$database->real_escape_string($_REQUEST["descricao"]).'"';
$sigla = '"'.$database->real_escape_string($_REQUEST["sigla"]).'"';
//$servico = '"'.$database->real_escape_string($_REQUEST["servico"]).'"';
//$iddepartamento = '"'.$database->real_escape_string($_REQUEST["iddepartamento"]).'"';
//$idpalavra_chave = '"'.$database->real_escape_string($_REQUEST["idpalavra_chave"]).'"';
$idusuario = '"'.$database->real_escape_string($_REQUEST["idusuario"]).'"';
$coord_name = '"'.$database->real_escape_string($_REQUEST["coord_name"]).'"';
$coord_lattes = '"'.$database->real_escape_string($_REQUEST["coord_lattes"]).'"';
//$oldDep = '"'.$database->real_escape_string($_REQUEST["oldDepartment"]).'"';
//$oldInventor = '"'.$database->real_escape_string($_REQUEST["oldinventor"]).'"';
//$oldKey = '"'.$database->real_escape_string($_REQUEST["oldKey"]).'"';
$idlab = '"'.$database->real_escape_string($_REQUEST["idlab"]).'"';

$idInventorArray = explode('|',$_REQUEST["idinventor"]);
$idDepartamentoArray = explode('|',$_REQUEST["iddepartamento"]);
$idKeyWordArray = explode('|',$_REQUEST["idpalavra_chave"]);

$equipamentos = json_decode($_REQUEST["equipamentos"],true);
$servicos = json_decode($_REQUEST["servicos"],true);

$editrow = $database->query("UPDATE laboratorio SET nome=".$name.", descricao=".$desc.", sigla=".$sigla.", coord_nome=".$coord_name.", coord_link=".$coord_lattes." ,usuario_idusuario=".$idusuario." WHERE idlaboratorio=".$idlab);
if($editrow){
  echo "Sucesso: editrow corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}

$editDepartamento = $database->query("DELETE FROM laboratorio_has_departamento WHERE laboratorio_idlaboratorio=".$idlab);
for ($i = 0; $i < count($idDepartamentoArray) -1 ; $i++) { 
  $iddepartamento = '"'.$database->real_escape_string($idDepartamentoArray[$i]).'"';
  $insertDepartamento = $database->query("INSERT INTO laboratorio_has_departamento (laboratorio_idlaboratorio,departamento_iddepartamento) VALUES (".$idlab.",".$iddepartamento.")");
}

$editInventor = $database->query("DELETE FROM laboratorio_has_inventor WHERE laboratorio_idlaboratorio=".$idlab);
for ($i = 0; $i < count($idInventorArray) -1 ; $i++) {
  $idinventor = '"'.$database->real_escape_string($idInventorArray[$i]).'"';
  $insertInventor = $database->query("INSERT INTO laboratorio_has_inventor (laboratorio_idlaboratorio, inventor_idinventor) VALUES (".$idlab.",".$idinventor.")");
}


$editPalavraChave = $database->query("DELETE FROM palavra_chave_has_laboratorio WHERE laboratorio_idlaboratorio=".$idlab);
for ($i = 0; $i < count($idKeyWordArray) -1 ; $i++) {
  $idpalavra_chave = '"'.$database->real_escape_string($idKeyWordArray[$i]).'"';
  $insertInventor = $database->query("INSERT INTO palavra_chave_has_laboratorio (laboratorio_idlaboratorio, palavra_chave_idpalavra_chave) VALUES (".$idlab.",".$idpalavra_chave.")");
}

$editServico = $database->query("DELETE FROM servicos WHERE idlaboratorio=".$idlab);
$editsub = $database->query("DELETE FROM subservico WHERE idlaboratorio=".$idlab);
for($i = 0; $i < count($servicos); $i++)
{   
    $temp = '"'.$database->real_escape_string($servicos[$i]["name"]).'"';
    $insertServico = $database->query("INSERT INTO servico (servico, idlaboratorio) VALUES (".$temp.",".$idlab.")");    
    $lastServico = $database->insert_id; 
    for($j = 0; $j < count($servicos[$i]["subname"]); $j++)
    {
        $temp = '"'.$database->real_escape_string($servicos[$i]["subname"][$j]).'"';
        $insertSubServico = $database->query("INSERT INTO subservico (subservico, idservico, idlaboratorio) VALUES (".$temp.",".$lastServico.",".$idlab.")");
        print_r($servicos[$i]["subname"][$j]);
    }
}

$editEquip = $database->query("DELETE FROM equipamento WHERE idlaboratorio=".$idlab);
for($i = 0; $i < count($equipamentos); $i++)
{   
    $temp = '"'.$database->real_escape_string($equipamentos[$i]).'"';
    $insertEquip = $database->query("INSERT INTO equipamento (equipamento, idlaboratorio) VALUES (".$temp.",".$idlab.")");    
}
