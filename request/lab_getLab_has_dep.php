<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();

$id = '"'.$database->real_escape_string($_REQUEST["id"]).'"';


$labs = $database->parseToJSON("SELECT departamento_iddepartamento, departamento.nome FROM laboratorio_has_departamento INNER JOIN departamento ON laboratorio_has_departamento.departamento_iddepartamento = departamento.iddepartamento WHERE laboratorio_idlaboratorio=".$id);

?>