<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();
//Busca palavras-chaves que estão na categoria pesquisada
$keys = $database->parseToJSON("SELECT palavra_chave.idpalavra_chave, palavra_chave.nome, palavra_chave_has_laboratorio.laboratorio_idlaboratorio FROM palavra_chave LEFT join
 palavra_chave_has_laboratorio on palavra_chave.idpalavra_chave = palavra_chave_has_laboratorio.palavra_chave_idpalavra_chave");

?>