<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

print_r($_REQUEST);
$database = new Database();
$security = new Security();

$name = '"'.$database->real_escape_string($_REQUEST["name"]).'"';
$desc = '"'.$database->real_escape_string($_REQUEST["desc"]).'"';
$sigla = '"'.$database->real_escape_string($_REQUEST["sigla"]).'"';
//$servico = '"'.$database->real_escape_string($_REQUEST["servico"]).'"';
$coord_nome = '"'.$database->real_escape_string($_REQUEST["coord_name"]).'"';
$coord_lattes = '"'.$database->real_escape_string($_REQUEST["coord_lattes"]).'"';
$telefone = '"'.$database->real_escape_string($_REQUEST["telefone"]).'"';
$contato = '"'.$database->real_escape_string($_REQUEST["contato"]).'"';
$email = '"'.$database->real_escape_string($_REQUEST["email"]).'"';
$endereco = '"'.$database->real_escape_string($_REQUEST["endereco"]).'"';
$cidade = '"'.$database->real_escape_string($_REQUEST["cidade"]).'"';
$UF = '"'.$database->real_escape_string($_REQUEST["UF"]).'"';
$CEP = '"'.$database->real_escape_string($_REQUEST["CEP"]).'"';
//$iddepartamento = '"'.$database->real_escape_string($_REQUEST["iddepartamento"]).'"';
//$idpalavra_chave = '"'.$database->real_escape_string($_REQUEST["idpalavra_chave"]).'"';
$idDepartamentoArray = explode('|',$_REQUEST["iddepartamento"]);
$idInventorArray = explode('|',$_REQUEST["idinventor"]);
$idPalavraArray = explode('|',$_REQUEST["idpalavra_chave"]);

$servicos = json_decode($_REQUEST["servicos"],true);
$equipamentos = json_decode($_REQUEST["equipamentos"],true);
print_r($servicos);
$idusuario = '"'.$database->real_escape_string($_REQUEST["idusuario"]).'"';

$insert_row = $database->query("INSERT INTO laboratorio (nome, descricao, sigla, coord_nome, coord_link, usuario_idusuario, telefone, contato, email, endereco, cidade, UF, CEP) VALUES (".$name.",".$desc.",".$sigla.",".$coord_nome.",".$coord_lattes.",".$idusuario.",".$telefone.",".$contato.",".$email.",".$endereco.",".$cidade.",".$UF.",".$CEP.")");
//$statement = $database->prepare($insert_row);

$filePath = "C:/xampp/htdocs/tempo/".$_REQUEST['fileName'];


for($i = 0; $i < count($servicos); $i++)
{
    print_r($servicos[$i]["name"]);
    for($j = 0; $j < count($servicos[$i]["subname"]); $j++)
    {
        print_r($servicos[$i]["subname"][$j]);
    }
}


if($insert_row){
    $lastId = $database->insert_id;
    //$insertPalavra = $database->query("INSERT INTO palavra_chave_has_laboratorio (palavra_chave_idpalavra_chave, laboratorio_idlaboratorio) VALUES (".$idpalavra_chave.",".$lastId.")");
    for ($i = 0; $i < count($idInventorArray) -1 ; $i++) {
        echo($lastId);
		$idinventor = '"'.$database->real_escape_string($idInventorArray[$i]).'"';
    	$insertInventor = $database->query("INSERT INTO laboratorio_has_inventor (laboratorio_idlaboratorio, inventor_idinventor) VALUES (".$lastId.",".$idinventor.")");
    }
    for ($i = 0; $i < count($idDepartamentoArray) -1 ; $i++) { 
        echo($lastId);
		$iddepartamento = '"'.$database->real_escape_string($idDepartamentoArray[$i]).'"';
    	$insertDepartamento = $database->query("INSERT INTO laboratorio_has_departamento (laboratorio_idlaboratorio, departamento_iddepartamento) VALUES (".$lastId.",".$iddepartamento.")");
    }
    //$insertPalavra = $database->query("INSERT INTO tecnologia_has_palavra_chave (tecnologia_idtecnologia, palavra_chave_idpalavra_chave) VALUES (".$lastId.",".$idpalavra_chave.")");
    for ($i = 0; $i < count($idPalavraArray) -1 ; $i++) {
		$idpalavra_chave = '"'.$database->real_escape_string($idPalavraArray[$i]).'"';
    	$insertPalavra = $database->query("INSERT INTO palavra_chave_has_laboratorio (palavra_chave_idpalavra_chave, laboratorio_idlaboratorio) VALUES (".$idpalavra_chave.",".$lastId.")");
    }
    // Salvar serviços e subserviços associados ao laboratório
    for($i = 0; $i < count($servicos); $i++)
    {   
        $temp = '"'.$database->real_escape_string($servicos[$i]["name"]).'"';
        $insertServico = $database->query("INSERT INTO servico (servico, idlaboratorio) VALUES (".$temp.",".$lastId.")");    
        $lastServico = $database->insert_id; 
        for($j = 0; $j < count($servicos[$i]["subname"]); $j++)
        {
            $temp = '"'.$database->real_escape_string($servicos[$i]["subname"][$j]).'"';
            $insertSubServico = $database->query("INSERT INTO subservico (subservico, idservico, idlaboratorio) VALUES (".$temp.",".$lastServico.",".$lastId.")");
            print_r($servicos[$i]["subname"][$j]);
        }
    }

    for($i = 0; $i < count($equipamentos); $i++)
    {   
        $temp = '"'.$database->real_escape_string($equipamentos[$i]).'"';
        $insertEquip = $database->query("INSERT INTO equipamento (equipamento, idlaboratorio) VALUES (".$temp.",".$lastId.")");    
    }
    if($filePath != "C:/xampp/htdocs/tempo/")
        $insertFile = $database->query("INSERT INTO laboratorio_arquivos VALUES ('$lastId','$filePath')");

}else{
    die('Error : ('. $database->errno .') '. $database->error);
}