<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

$database = new Database();
$security = new Security();
//Busca palavras-chaves que estão na categoria pesquisada
$keys = $database->parseToJSON("SELECT departamento.iddepartamento, departamento.nome, departamento.sigla, laboratorio_has_departamento.laboratorio_idlaboratorio FROM departamento LEFT join
 laboratorio_has_departamento on departamento.iddepartamento = laboratorio_has_departamento.departamento_iddepartamento");

?>