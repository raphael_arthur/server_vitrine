<?php
require_once "../class/User.class.php";
require_once "../class/Security.class.php";
require_once "../class/Database.class.php";

header("Access-Control-Allow-Origin: *");

print_r($_REQUEST);
$database = new Database();
$security = new Security();

$idtech = '"'.$database->real_escape_string($_REQUEST["id"]).'"';

$iddepartamento = $database->parseToJSON("SELECT departamento_iddepartamento FROM tecnologia_has_departamento WHERE tecnologia_idtecnologia=".$idtech);
$iddepartamento = '"'.$database->real_escape_string($iddepartamento).'"';

$idinventor = $database->parseToJSON("SELECT inventor_idinventor FROM tecnologia_has_inventor WHERE tecnologia_idtecnologia=".$idtech);
$idinventor = '"'.$database->real_escape_string($idinventor).'"';

$idpalavra_chave = $database->parseToJSON("SELECT palavra_chave_idpalavra_chave FROM tecnologia_has_palavra_chave WHERE tecnologia_idtecnologia=".$idtech);
$idpalavra_chave = '"'.$database->real_escape_string($idpalavra_chave).'"';
//$idprojeto = '"'.$database->real_escape_string($_REQUEST["idprojeto"]).'"';


//$editInventor = $database->query("DELETE FROM tecnologia_has_inventor WHERE (tecnologia_idtecnologia=".$idtech." && inventor_idinventor=".$idinventor.")");
$editInventor = $database->query("DELETE FROM tecnologia_has_inventor WHERE (tecnologia_idtecnologia=".$idtech.")");

if($editInventor){
  echo "Sucesso: delInventor corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}

//$editDepartamento = $database->query("DELETE FROM tecnologia_has_departamento WHERE (tecnologia_idtecnologia=".$idtech." && departamento_iddepartamento=".$iddepartamento.")");
$editDepartamento = $database->query("DELETE FROM tecnologia_has_departamento WHERE (tecnologia_idtecnologia=".$idtech.")");

if($editDepartamento){
  echo "Sucesso: delDepartamento corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}

//$editKey = $database->query("DELETE FROM tecnologia_has_palavra_chave WHERE (tecnologia_idtecnologia=".$idtech." && palavra_chave_idpalavra_chave=".$idpalavra_chave.")");
$editKey = $database->query("DELETE FROM tecnologia_has_palavra_chave WHERE (tecnologia_idtecnologia=".$idtech.")");

if($editKey){
  echo "Sucesso: delKey corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}

$editrow = $database->query("DELETE FROM tecnologia WHERE idtecnologia=".$idtech);
if($editrow){
  echo "Sucesso: delrow corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}

/*$editrow = $database->query("DELETE FROM tecnologia_has_laboratorio WHERE idtecnologia=".$idtech);
if($editrow){
  echo "Sucesso: delrow corretamente!";
}else{
    die('Error : ('. $database->errno .') '. $database->error);
}


//$statement = $database->prepare($insert_row);
