<?php

/*brief
	Guilherme natal 05/03/2018
	essa classe será usada para funcoes de email do aplicativo do vitrine
	para qualquer funcao de relacionada a servicos de email use essa classe

*/
class SendMail{

    public $subject = "Recuperacao de senha CDT vitrine";
	public $text = "Clique no link para recuperar sua senha"."\r\n".
	"http://localhost:8080/server_vitrine/request/PaginaRecuperacao.php";
    public $headers = "From: guilhermenatal47@gmail.com";


    /*
		guilherme natal 05/03/2018
		a função SendTo envia um email de recuperacao para $to
		o email e padronizado então esta usando variaveis ja definidas.
	
    */
    function SendTo($to){
        mail($to,$this->subject,$this->text,$this->headers);
    }
    
}

?>