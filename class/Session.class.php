<?php
/*
    Development of this system explanation.
    Obs: This class will wrap/destroy any session handler defined by the server and implements the session handlers defined in this class. In order to prevent "Access Denied Errors" from apache, the server data is storage (crypted) in the server database.
    
    Important things to remember:
    1. Call the garbage collector very often, deleted inactive session
    
    2. Generate a new sessionid for every time the user login
    
    3. DO NOT forget to lock's the session when it's open - if the conection was timed-out then that session will be destroyed.
        --How it's Works:
            -- If a script asks for writing in the Session data, fist lock the session -> write the data and then release the lock.
            -- The script that's trying to read/write the dada must wait ultin the lock is released. This prevent session data loss/overwriting.
	This class can not be called in this project
	because the server version is 5.3 we need a server version >= 5.4 for this to work properly
	using the standard session...
*/
require_once 'Database.class.php';
require_once 'Security.class.php';

class Session implements SessionHandlerInterface{
	
	private $sessionID;
    private $Database;
    private $sessionData;
    private $sessionLock;
    private $accessTime;
    
    private $sec;
    private $Key;
    
    //Session Key is secret, because of this theses set/get methdos will be private
    private function getKey($id){
    	if($this->checkSession($id)){
    		$key = $this->Database->query("SELECT session_key FROM session WHERE id = '$id'")->fetch_object()->session_key;
    		if($key == "" || $key == false || strlen($key) < 60){
    			$this->generateSessionKey();
    		} else if( gettype($key) == "string" and strlen($key) >= 60){
    			$this->Key = $key;
    		}
    	} else {
    		$this->generateSessionKey();
    	}
    	return $this->Key;
    }
    
    private function generateSessionKey(){
    	$this->Key = $this->sec->generateKey();
    }
    
    private function lockSession($id){
    	
    }
    private function unlockSession(){
    	
    }
    
    function __construct(){
    	$this->Database = new Database();
    	$this->sec = new Security();
    }
    
    public function open($savePath, $name){
        $this->accessTime = time();
        
        //print " <br /> Session Created Session name: " . $name . "<br />";
    }
    
    public function close(){
        //print "<br /> Closed Function Executed";
        //$this->Database->prepare("UPDATE session ")
    	$this->Database->close();
        return true;
    }
    
    public function read($id){
    	//print "<br /> read Function Executed: Session ID : " . $id . " <br />";
    	$this->getKey($id);
    	
    	$this->sessionID = $id;
    	$dataQuery = $this->Database->prepare("SELECT data FROM session WHERE id = ?");
        
		if($dataQuery !== false){
            $dataQuery->bind_param('s', $id);
            if($dataQuery->execute() !== false){
                $dataQuery->bind_result($this->sessionData);
                $dataQuery->fetch();        
                //debug
                //print " - Session Data: " . $this->sessionData;
                //print "Decrypted Data: " . $this->sec->decrypt($this->sessionData, $this->Key);
                return $this->sec->decrypt($this->sessionData, $this->Key);
            }
        } else {
            print $dataQuery->error();
            return false;
        }
    }
    
    public function write($id, $data){
    	//print "<br /> Write Function Executed <br />";
    	
    	if(!$this->Database->query("DELETE FROM session WHERE id = '$id'")){
    		print $this->Database->error;
    	}
    	
    	//chipher the session data
    	
    	$this->sessionData = $this->sec->encrypt($data, $this->Key);
    	
    	//print "Encrypted session Data: " . $this->sessionData;
    	
    	if(!$this->Database->query("INSERT INTO session (`id`,`time`,`data`,`session_key`) VALUES ('$id','$this->accessTime','$this->sessionData', '$this->Key') ")){
    		print $this->Database->error;
    	}
    }
    
    public function destroy($id){
    	//print "<br /> Destroy Function Executed";
    	$dataQuery = $this->Database->prepare("DELETE FROM session WHERE id = ?");
    	
    	if($dataQuery !== false){
    		$dataQuery->bind_param('s', $id);
    		$dataQuery->execute();
    	} else {
    		return false;
    	}
    	
        return true;
    }
    
    public function gc($maxlifetime){
    	$dataQuery = $this->Database->prepare("DELETE FROM session WHERE time < ?");
    	if($dataQuery !== false){
    		$dataQuery->bind_param('s', $this->accessTime - $maxlifetime);
    		if($dataQuery->execute()){
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		return false;
    	}
    }
    
    public function checkSession($id){
    	$call = $this->Database->query("SELECT id FROM session WHERE id = '$id'")->fetch_object();
		$storedId = "";
		
		if($call != false) {
			$storedId = $call->id;
		}
    	
		if($storedId !== "" && $storedId == $id){
    		//print "Session exists: " . $storedId . " actual id: " . $id;
    		return true;
    	} else {
    		return false;
    	}
    }
}

?>