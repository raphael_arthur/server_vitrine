<?php
/*
	 *  Extends mysqli to connect to the database, create an instance of this every time we need
     *  an request to mysql
	 *  @param (__construct) conection information
	 */

error_reporting(E_ALL);
    ini_set('display_errors', 1);

class Database extends mysqli {
	/*
	private $ServerIP = "10.10.10.38";
	private $DatabaseUser = "root";
	private $DatabasePassword = "*CDTUNB2012";
	private $DatabaseAlias = "vitrine";
	*/

	/*private $ServerIP = "10.10.10.49";
	private $DatabaseUser = "root";
	private $DatabasePassword = "*CDTUNB2012";
	private $DatabaseAlias = "plataforma_eventos";
	
	private $ServerIP = "localhost";
	private $DatabaseUser = "root";
	private $DatabasePassword = "guiga@11";
	private $DatabaseAlias = "vitrine";
	*/

	private $ServerIP = "localhost";
	private $DatabaseUser = "root";
	private $DatabasePassword = "guiga@11";
	private $DatabaseAlias = "vitrine";
	
	//   private $ServerIP = "172.16.4.21";
	//   private $DatabaseUser = "petmat";
	//   private $DatabasePassword = "D67N?9i!34";
	//   private $DatabaseAlias = "petmat";

	private $charset = "utf-8";

	function __construct(){
		parent::__construct($this->ServerIP, $this->DatabaseUser, $this->DatabasePassword, $this->DatabaseAlias);
        $this->set_charset($this->charset);
		// If connection fails
		if($this->connect_error){
			print "Database connection error!" . " Error Number: " . $this->connect_errno;
		}
	}

	// Execute a Query and print the result as a JSON text
	public function parseToJSON($query){
		$result = $this->query($query);
		$array = array();

		while($row = $result->fetch_assoc()){
			$array[] = $row;
		}

		echo json_encode($array);
	}

	public function parseToJSON2($query){
		$result = $this->query($query);
		$array = array();
		/*verifica se query não esta retornando vazio*/
		if($result){

			while($row = $result->fetch_assoc()){
				$array[] = $row;
			}
		}
		return json_encode($array);
	}

	// Function to Select a Singleton from the database
    //use this to respond to a single request from the client
	public function selectSingleton($query,$data){
		$call = $this->query($query);
        if($call->num_rows > 0){
			return $call->fetch_object()->$data;
        } else {
			return null;
		}
	}

	public function select($query){
		$call = $this->query($query);
        if($call->num_rows > 0){
			return $call->fetch_object();
        } else {
			return null;
		}
	}
}
?>
